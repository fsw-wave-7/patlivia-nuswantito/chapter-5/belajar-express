const {successResponse} = require("../helper/response.js");

class Post {
	constructor() {
		this.post = [
			{
				text: "Postingan pertama saya",
				created_at: "2021-03-11 21:10:00",
				username: "tito",
			},
			{
				text: "Postingan kedua saya",
				created_at: "2021-03-11 22:10:00",
				username: "tito",
			},
		];
	}

	getPost = (req, res) => {
		successResponse(res, 200, this.post, {total: this.post.length});
	};

	getDetailPost = (req, res) => {
		let index = req.params.index;
		successResponse(res, 200, this.post[index]);
	};

	insertPost = (req, res) => {
		let body = req.body;
		console.log(body);
		let newPost = {
			text: body.text,
			created_at: "2021-03-11 21:10:00",
			username: body.username,
		};

		this.post.push(newPost);
		successResponse(res, 201, this.post);
	};

	updatePost = (req, res) => {
		let index = req.params.index;
		let body = req.body;
		this.post[index].text = body.text;
		this.post[index].user = body.username;
		successResponse(res, 200, this.post[index]);
	};

	deletePost = (req, res) => {
		let index = req.params.index;
		this.post.splice(index, 1);
		successResponse(res, 200, this.post[index]);
	};
}

module.exports = Post;
