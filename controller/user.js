const {successResponse} = require("../helper/response.js");

class User {
	constructor() {
		this.user = [
			{
				nama_lengkap: "Tito Rollis",
				username: "Tito",
				password: "qwerty",
				email: "ouvalres@ouvalres.com",
			},
		];
	}

	getUser = (req, res) => {
		successResponse(res, 200, this.user, {total: this.user.length});
	};

	getDetailUser = (req, res) => {
		let index = req.params.index;
		successResponse(res, 200, this.user[index]);
	};

	insertUser = (req, res) => {
		let body = req.body;
		let newUser = {
			nama_lengkap: body.nama_lengkap,
			username: body.username,
			password: body.password,
			email: body.email,
		};

		this.user.push(newUser);
		successResponse(res, 201, this.user);
	};

	updateUser = (req, res) => {
		let body = req.body;
		let index = req.params.index;
		this.user[index].username = body.username;
		this.user[index].password = body.password;
		this.user[index].email = body.email;
		successResponse(res, 200, this.user[index]);
	};

	deleteUser = (req, res) => {
		let index = req.params.index;
		this.user.splice(index, 1);
		successResponse(res, 200, this.user[index]);
	};
}

module.exports = User;
