// buat CRUD
// data user: -nama lengkap, username, pass, email


const messages = {
	200: "Sukses",
	201: "Data berhasil disimpan",
};

function successResponse(res, code, data, meta = {}) {
	res.status(code).json({
		data: data,
		meta: {
			code: code,
			message: messages[code.toString()],
			...meta,
		},
	});
}

module.exports = {successResponse};
