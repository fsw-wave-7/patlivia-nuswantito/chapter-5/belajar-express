const express = require("express");
const app = express();
const morgan = require("morgan");
const fs = require("fs");
const port = 3000;
const bodyParser = require("body-parser");
const jsonParses = bodyParser.json();
const path = require("path");
const Post = require("./controller/post.js");
const post = new Post();
const User = require("./controller/user");
const user = new User();

app.use(morgan("tiny"));
app.use(express.static(__dirname + "/public"));

app.get("/", function (req, res) {
	res.render(path.join(__dirname, "./views/index"));
});

// Post
app.get("/post", (req, res) => {
	res.send(fs.readFileSync("./data/post.json"));
});
app.get("/post/:index", post.getDetailPost);
// Insert Post
app.post("/post", jsonParses, (req, res) => {
	let data = req.body;
	let postData = JSON.parse(fs.readFileSync("./data/post.json", "utf8"));
	postData.push(data);
	fs.writeFileSync("./data/post.json", JSON.stringify(postData));
});
// End of Insert Post
// put
app.put("/post/:index", jsonParses, post.updatePost);
// delete
app.delete("/post/:index", post.deletePost);
app.listen(port, () => {
	console.log("Server berhasil dijalankan");
});

// User
app.get("/user", (req, res) => {
	res.send(fs.readFileSync("./data/user.json"));
});
// Detail
app.get("/user/:index", user.getDetailUser);
// Insert
app.post("/user", jsonParses, (req, res) => {
	let data = req.body;
	let userData = JSON.parse(fs.readFileSync("./data/user.json", "utf8"));
	userData.push(data);
	fs.writeFileSync("./data/user.json", JSON.stringify(userData));
});
// Update
app.put("/user/:index", jsonParses, user.updateUser);
// Delete
app.post("/user/:index", user.deleteUser);
